package main;

import java.io.FileWriter;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * A interface specifies the operations on Student object
 * 
 * @author praveena prasannakumar
 *
 * 
 */
public interface PhoneService {
	/**
	 * The method will add a phone
	 * 
	 * @param phone to be added
	 * 
	 */
	void add(Phone phone);
	/**
	 * The method will  delete phone
	 * 
	 * @param phone to be deleter
	 * 
	 */
	void delete(String name);


	/**
	 * The method should display all the phones
	 */

	void displayElements();

	/**
	 * The method will search phones and display the phone with given name
	 * 
	 * @param name name of the phone to be searched
	 * 
	 */

	void search(String name);

	/**
	 * The method sorts the phones based on name
	 */

	void sort();

	/**
	 * The method will search phones and display the manufactured date with
	 * manufactured date is after the given manufactured date
	 * 
	 * @param manufactured date
	 * 
	 */

	void search(LocalDate date);
	
	/**
	 * The method will search phones and display the manufactured date with
	 * manufactured date is after the given manufactured date
	 * with return type list 
	 * @param manufactured date
	 * 
	 */

	List<Phone> searchDate(LocalDate d1);
	

	/**
	 * This default method getCSVString append the data and return the data as csv
	 * format
	 */
	void downloadDetailsAsCSV() throws Exception;
	
	default String getCSVString(Phone phone) { // comma for csv values
		return new StringBuffer(phone.getName()).append("\",").append(phone.getYear()).append(",")
				.append(phone.getPrice()).append(",").append(phone.getRam()).append(",").append(phone.getColor())
				.append("\",").append(phone.getManufaturedDate()).append(",").toString();

	}
	

	/**
	 * This default method getJsonString append the data and return the data as json
	 * format
	 * @throws Exception 
	 */
	void downloadDetailsAsJSON() throws Exception;
	
	default String getJSONString(Phone phone) { 
		return new StringBuffer().append("{").append("\"Phone Name\":").append("\"").append(phone.getName()).append("\",")
				.append("\n").append("\"Year\":").append(phone.getYear()).append(",").append("\n").append("\"Phone Price\":")
				.append(phone.getPrice()).append(",").append("\n").append("\"Ram\":").append(phone.getRam()).append(",")
				.append("\n").append("\"Phone color\": \"").append(phone.getColor()).append("\",").append("\n").append("\"Manufature date\":")
				.append(phone.getManufaturedDate()).append("}").toString();

	}	
	
}
