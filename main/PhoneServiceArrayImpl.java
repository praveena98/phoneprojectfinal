package main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.Phone;
import main.PhoneServiceArrayImpl;

/**
 * A interface specifies the operations on phone object
 * 
 * @author praveena prasannakumar
 *
 *
 */

public class PhoneServiceArrayImpl implements PhoneService {

	private Phone[] phones;
	private int j = 0;
	
	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}
	
	//constructor
	public PhoneServiceArrayImpl() {
		phones=new Phone[100];
		
	}
	
	//constructor accepts array size
	public PhoneServiceArrayImpl(int size) {
		phones=new Phone[size];
		
	}
		
	/**
	 * The method will add a phone
	 * 
	 * @param phone phone to be added
	 *
	 */

	@Override
	public void add(Phone phone) { 
		// TODO Auto-generated method stub
		boolean flag = true;
		if (j == phones.length) {
			System.out.println("size is exceeded");
		} else{
			for (int i = 0; i < j; i++) {
				if (phones[i].equals(phone)) {
					System.out.println("duplicate alert!!!");
					flag = false;
					// break
				}
			}
			if (flag == true) {
				phones[j] = phone;
				j++;
			}
		}	
	}

	/*
	 * The Method will Search for Phone
	 * 
	 * @param name
	 */

	public void search(String name) {
		boolean flag = false;
		for (int i = 0; i < j; i++) {
			if (phones[i].getName().equalsIgnoreCase(name)) {
				System.out.println(phones[i]);
				flag = true;
			}
		}
		if (flag == false) {
			System.out.println("no results found");
		}
	}

	/* The Method will Sorting the Phone Details */
	public void sort() {
		Arrays.sort(phones, 0, j);
		displayElements();
	}

	/* The Method will Search the Manufatured date of phone */
	public void search(LocalDate date) {
		boolean flag = false;
		for (int i = 0; i < j; i++) {
			if (phones[i].getmanufaturedDate().isAfter(date)) {
				System.out.println(phones[i]);
				flag = true;
			}
		}
		if (flag == false) {
			System.out.println("no results found");
		}
	}

//Display the phones
	@Override
	public void displayElements() {
		// TODO Auto-generated method stub
		if (j == 0) {
			System.out.println("no data");
		} else {
			for (int i = 0; i < j; i++) {
				System.out.println(phones[i]);
			}
		}
	}

	/**
	 * The method downloadDetailsAsCSV download the data as csv format
	 */

	public void downloadDetailsAsCSV() {
		// TODO Auto-generated method stub
		try {
			String data;
			FileWriter file = new FileWriter("downloadPhone.txt");
			for (int i = 0; i < j; i++) {
				data = getCSVString(phones[i]);
				System.out.println(data);
				file.write(data);
				// file.flush();
			}
			file.close();
		} catch (Exception exception) {
			System.out.println(exception.getMessage());

		}
	}

	/**
	 * The method downloadDetailsAsJSON download the data as json format
	 */

	public void downloadDetailsAsJSON() throws IOException {
		// TODO Auto-generated method stub

		FileWriter file = new FileWriter("downloadPhoneJson.txt");
		for (int i = 0; i < j; i++) {
			file.write(getJSONString(phones[i]));
			file.flush();
			file.close();
		}
	}

	@Override
	public void delete(String name) { 
		// TODO Auto-generated method stub
		int k = 0;
		for (int i = 0; i < j; i++) {
			if (!phones[j].getName().equals(name)) {
				phones[k] = phones[i];
				k++;
				System.out.println("Successfully deleted");
			}
		}
		j = k;
	}
		

	@Override
	public List<Phone> searchDate(LocalDate date) {
		// TODO Auto-generated method stub
			List<Phone>phoneList=new ArrayList<Phone>();
			for(int i=0;i<j;i++) {		
				if(phones[i].getManufaturedDate().isAfter(date)) {
					phoneList.add(phones[i]);
					displayElements();
				}
			}
			return phoneList;	
	}
}
