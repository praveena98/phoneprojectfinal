package main;
import java.util.Properties;


public abstract class PhoneServiceFactory {

	public static PhoneService getPhoneService(Properties properties) {
		String type=properties.getProperty("implementation");
		type=type.toUpperCase();
		PhoneService phoneServiceFact=null;
		switch(type) {
		case "ArrayImpl":
			phoneServiceFact=new PhoneServiceArrayImpl();
			break;
		case "ArrayListImpl":
			phoneServiceFact=new PhoneServiceArrayListImpl();
		case "DATABASE":	
			String user=properties.getProperty("user");
			String driver=properties.getProperty("driver");
			String url=properties.getProperty("url");
			String password=properties.getProperty("password");
			phoneServiceFact=new PhoneServiceDatabaeImplementation(driver,url,user,password);
			break;					
	}
	return phoneServiceFact;		
	}

	

}
