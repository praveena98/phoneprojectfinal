package main;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;

public class PhoneServiceDatabaeImplementation implements PhoneService {
	String driver, url, user, password;
	// private List<Phone> phoneList;

	// constructor by eclipse
	public PhoneServiceDatabaeImplementation(String driver, String url, String user, String password) {
		this.driver = driver;
		this.url = url;
		this.user = user;
		this.password = password;
	}

	public PhoneServiceDatabaeImplementation() {
		// TODO Auto-generated constructor stub
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		return DriverManager.getConnection(url, user, password);

	}
	private Phone getPhone(ResultSet resultSet) throws SQLException {
		Phone phone = new Phone();
		phone.setName(resultSet.getString("name"));
		phone.setYear(resultSet.getInt("year"));
		phone.setPrice(resultSet.getDouble("price"));
		phone.setRam(resultSet.getInt("ram"));
		phone.setColor(Color.valueOf(resultSet.getString("phone_Color")));
		phone.setManufaturedDate(resultSet.getDate("manufatured_date").toLocalDate());
		return phone;
	}

	@Override
	public void add(Phone phone) {
		// TODO Auto-generated method stub
		try {
			Connection connection = getConnection();
			String query = "INSERT INTO phone(name,year,price,ram,phone_color,manufatured_date)VALUES(?,?,?,?,?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, phone.getName());
			preparedStatement.setInt(2, phone.getYear());
			preparedStatement.setDouble(3, phone.getPrice());
			preparedStatement.setInt(4, phone.getRam());
			preparedStatement.setString(5, phone.getColor().toString());
			preparedStatement.setDate(6, Date.valueOf(phone.getmanufaturedDate()));
			preparedStatement.executeUpdate();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		System.out.println();
		System.out.println("------Data added successfully-----");
	}

	@Override
	public void delete(String name) {
		// TODO Auto-generated method stub
		try {
			Connection connection = getConnection();
			String query = "DELETE FROM phone WHERE name = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, name);
			preparedStatement.executeUpdate();
			System.out.println("-----Deleted successfully-----");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void displayElements() {
		// TODO Auto-generated method stub
		try {
			Connection connection = getConnection();
			String query = "SELECT * FROM phone";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Phone phone = getPhone(resultSet);
				System.out.println(phone);
				System.out.println();

			}
			resultSet.close();
			preparedStatement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		System.out.println("------Displayed successfully------");
	}

	@Override
	public void search(String name) {
		// TODO Auto-generated method stub
		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM phone where name=?");
			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Phone phone = getPhone(resultSet);
				System.out.println(phone);
				System.out.println();
			}
			resultSet.close();
			preparedStatement.close();
			connection.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		System.out.println("------Searched phone successfully-------");

	}

	@Override
	public void sort() {
		// TODO Auto-generated method stub
		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM phone ORDER by price ASC");

			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Phone phone = getPhone(resultSet);
				System.out.println(phone);
			}
			resultSet.close();
			preparedStatement.close();
			connection.close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("------Sorted successfully------");

	}

	@Override
	public void search(LocalDate date) {
		// TODO Auto-generated method stub
		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement prepareStatement = connection.prepareStatement("SELECT * FROM phone where manufatured_date=?");
			prepareStatement.setDate(1, Date.valueOf(date));
			ResultSet resultset = prepareStatement.executeQuery();
			while (resultset.next()) {
				while (resultset.next()) {
					Phone phone = getPhone(resultset);
					System.out.println(phone);
				}
			}
			connection.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		System.out.println("-------Successfully searched Date-------");

	}

	@Override
	public List<Phone> searchDate(LocalDate d1) {    
		// TODO Auto-generated method stub
		List<Phone> phoneList = new ArrayList<Phone>();
		Phone phoneObj = new Phone();
		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM phone where manufatured_date=?");
			preparedStatement.setDate(1, Date.valueOf(d1));
			ResultSet resultset = preparedStatement.executeQuery();
//			List<Phone> phoneList = new ArrayList<Phone>();
			while (resultset.next()) {
				phoneObj = getPhone(resultset);
				phoneList.add(phoneObj);
			}
			resultset.close();
			preparedStatement.close();
			connection.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return phoneList;
	}

	@Override
	public void downloadDetailsAsCSV() throws Exception {
		// TODO Auto-generated method stub

		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM phone");
			ResultSet resultSet = preparedStatement.executeQuery();
			FileWriter file = new FileWriter("DatabaseCSV_downloadPhone.txt");
			while (resultSet.next()) {

				String data = getCSVString(getPhone(resultSet));
				file.write(data);
			}
			file.close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("-----Successfully downloaded the CSV-----");

	}

	@Override
	public void downloadDetailsAsJSON() throws Exception {
		// TODO Auto-generated method stub
		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM phone");
			ResultSet resultSet = preparedStatement.executeQuery();
			FileWriter file = new FileWriter("DatabaseJSON_downloadPhone.txt");
			StringJoiner join = new StringJoiner(",", "[", "]");
			while (resultSet.next()) {

				String data = join.add(getJSONString(getPhone(resultSet))).toString();
				file.write(data);
			}
			file.close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("---------Successfully downloaded the json-------");

	}

}
