package main;

import java.io.FileReader;
import java.time.LocalDate;
import java.util.Properties;
import java.util.Scanner;


public class MenuClass {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int ch;
		//PhoneService phoneService = new PhoneServiceImpl(); 
		//PhoneService phoneService= new PhoneServiceArrayListImpl();	
		//System.out.println("Choose:\n1.ArrayImpl\n2.ArrayListImpl");	
		FileReader reader=new FileReader("main/config.properties");
		Properties properties=new Properties();
		properties.load(reader);
		
		PhoneService phoneService=PhoneServiceFactory.getPhoneService(properties);
		//PhoneService phoneService = new PhoneServiceFactory.getPhoneService("ArrayList");
				
		do {
			System.out.println("Enter the choice:");
			System.out.println("\t1.Add");
			System.out.println("\t2.Display");
			System.out.println("\t3.Search");
			System.out.println("\t4.Sort");
			System.out.println("\t5.Delete");
			System.out.println("\t6.SearchManufatureDate");
			System.out.println("\t7.Download Phone as CSV");
			System.out.println("\t8.Download Phone ac JSON");
			System.out.println("\t9.Exit");
			ch = scanner.nextInt();
			switch (ch) {
			case 1:
				try {
					System.out.println("Enter name of the PHONE :");
					String name = scanner.next();
					System.out.println("Enter  YEAR :");
					int year = scanner.nextInt();
					System.out.println("Enter PRICE :");
					double price = scanner.nextDouble();
					System.out.println("Enter RAM :");
					int ram = scanner.nextInt();

					System.out.println("Enter Phone Color :");
					Color phonecolor = Color.valueOf(scanner.next().toUpperCase());

					System.out.println("Enter DATE :");
					String date = scanner.next();
					LocalDate d = LocalDate.parse(date);
					Phone phones = new Phone(name, year, price, ram, phonecolor, d);
					phoneService.add(phones);

				} catch (Exception exception) {
					System.out.println(exception.getMessage());
				}
				break;
			case 2:
				phoneService.displayElements();
				break;
			case 3:
				System.out.println("serach element:");
				String name = scanner.next();
				phoneService.search(name);
				break;
			case 4:
				phoneService.sort();
				break;
			case 5:
				System.out.println("Enter phone to be deleted:");
				String phName = scanner.next();
				phoneService.delete(phName);
				break;
			case 6:
				System.out.println("Enter date to be checked");
				String checkDate = scanner.next();
				LocalDate d = LocalDate.parse(checkDate);
				phoneService.search(d);
				System.out.println();
			case 7:
				try {
				phoneService.downloadDetailsAsCSV();
				}
				catch(Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 8:
				try {
					phoneService.downloadDetailsAsJSON();
					}
					catch(Exception e) {
						System.out.println(e.getMessage());
					}
				
				break;
			case 9:
				System.exit(0);
				break;
		
			default:
				System.out.println("invalid choice");
				break;
			}
		} while (ch != 9);

		scanner.close();

	}

}