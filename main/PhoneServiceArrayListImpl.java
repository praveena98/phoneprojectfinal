package main;

import java.util.*;
import java.util.stream.Collectors;
import java.awt.Color;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;

public class PhoneServiceArrayListImpl implements PhoneService {

	List<Phone> phoneList;
	int j = 0;

	// constructor
	public PhoneServiceArrayListImpl() {
		phoneList = new ArrayList<>();

	}

	@Override
	public void add(Phone phone) {
		if (phoneList.size() > 0 && phoneList.contains(phone)) {
			System.out.println("Already exist");

		} else {
			phoneList.add(phone);
		}
	}

	@Override
	public void displayElements() {
		// TODO Auto-generated method stub
		// forEach method
		for (Phone data : phoneList)
			System.out.println("Phone Details\n" + data);

	}

	@Override
	public void search(String name) {
		// TODO Auto-generated method stub
		boolean flag = false;
		for (Phone element : phoneList)
			if (element.getName().equalsIgnoreCase(name)) {
				System.out.println(element);
				flag = true;
			}
		if (flag == false) {
			System.out.println("no results found");
		}

	}

	@Override
	public void sort() {
		// TODO Auto-generated method stub
		Collections.sort(phoneList);
		displayElements();

	}

	@Override
	public void downloadDetailsAsCSV() {
		// TODO Auto-generated method stub
		try {
			FileWriter file = new FileWriter("downloadPhone.txt");
			for (Phone data : phoneList) {
				file.write(getCSVString(data));
				System.out.println(data);
			}
			file.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void downloadDetailsAsJSON() throws IOException {
		// TODO Auto-generated method stub
		FileWriter file = new FileWriter("downloadPhoneJson.txt");
		for (Phone data : phoneList) {
			file.write(getJSONString(data));
			file.flush();
			file.close();
		}

	}

//	@Override
//	public void delete(String name) {             // single loop
//		// TODO Auto-generated method stub
//			for (int i = 0; i < phoneList.size(); i++) {
//				if (phoneList.get(i).getName().equalsIgnoreCase(name)) {
//					phoneList.remove(i);
//					System.out.println("Successfully deleted");
//				}
//			}
//		}
//	@Override
//	public void delete(String name) {
//		// TODO Auto-generated method stub
//		Iterator<Phone> iterator = phoneList.iterator();
//		Phone phone = iterator.next();
//		if (phone.getName().equals(name)) {
//			iterator.remove();
//		}
//	}
	public void delete(String type) {
		for (Iterator<Phone> itr = phoneList.iterator(); itr.hasNext();) {
			Phone name = itr.next();
			if (name.getName().equals(type)) {
				itr.remove();
			}
		}
		System.out.println("DELETED SUCESSFULLY");
	}

	@Override
	public List<Phone> searchDate(LocalDate date) {
		// TODO Auto-generated method stub
		return phoneList.stream().filter(phoneList -> phoneList.getmanufaturedDate().isAfter(date))
				.collect(Collectors.toList());
	}

	@Override
	public void search(LocalDate date) {
		// TODO Auto-generated method stub

	}

}
