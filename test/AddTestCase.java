package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import main.Color;
import main.Phone;
import main.PhoneServiceArrayImpl;

class AddTestCase {
	PhoneServiceArrayImpl phoneServiceArrayImpl=new PhoneServiceArrayImpl(3);
	
	@Test
	void test() {

		//PhoneServiceArrayImpl phoneServiceArrayImpl=new PhoneServiceArrayImpl(1);
		phoneServiceArrayImpl.add(new Phone("redmi",2022,22000.0,3,Color.BLUE,LocalDate.parse("2022-19-10")));
		assertEquals(1,phoneServiceArrayImpl.getJ());
	}
	
	@Test
	public void size() {
		//PhoneServiceArrayImpl phoneServiceArrayImpl=new PhoneServiceArrayImpl(5);
		phoneServiceArrayImpl.add(new Phone("redmi",2021,22000.0,3,Color.BLUE,LocalDate.parse("2022-06-10")));
		phoneServiceArrayImpl.add(new Phone("vivo",2022,23000.0,2,Color.WHITE,LocalDate.parse("2022-19-10")));
		phoneServiceArrayImpl.add(new Phone("galaxy",2015,25000.0,5,Color.BLUE,LocalDate.parse("2022-09-10")));
		phoneServiceArrayImpl.add(new Phone("nokia",2019,26000.0,1,Color.BLACK,LocalDate.parse("2021-19-10")));
		assertEquals(4,phoneServiceArrayImpl.getJ());

	}
	
	@Test
	public void test1() {

		//PhoneServiceArrayImpl phoneServiceArrayImpl=new PhoneServiceArrayImpl();
		phoneServiceArrayImpl.displayElements();
		assertEquals(1, phoneServiceArrayImpl.getJ());
	}
		

//	@Test
//	public void SearchDate() {
//		
//		List<Phone> phoneList = new ArrayList<>();
//
//		if (phoneList.contains(phoneList)) {
//			assertTrue(true);
//		} else {
//			assertTrue(false);
//		}
//
//	}
//	


}
